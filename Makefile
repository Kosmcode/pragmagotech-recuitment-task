PHONY: test-unit
test-unit:
	./vendor/bin/phpunit --testsuite Unit

.PHONY: test-coverage
test-coverage:
	./vendor/bin/phpunit --coverage-html ./tests/coverage

.PHONY: init-and-run-tests
init-and-run-tests:
	composer install
	make test-unit