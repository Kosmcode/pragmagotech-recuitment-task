<?php

declare(strict_types=1);

namespace PragmaGoTech\Interview\Traits\Number;

/**
 * Trait of Number round
 */
trait RoundTrait
{
    /**
     * @param $valueToRound
     * @param $roundValue
     *
     * @return float|int
     */
    private function roundUpToAny($valueToRound, $roundValue = 5): float|int
    {
        return (ceil($valueToRound) % $roundValue === 0)
            ? ceil($valueToRound)
            : round(($valueToRound + $roundValue / 2) / $roundValue) * $roundValue;
    }
}