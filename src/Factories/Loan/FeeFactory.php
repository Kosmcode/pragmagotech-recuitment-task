<?php

declare(strict_types=1);

namespace PragmaGoTech\Interview\Factories\Loan;

use PragmaGoTech\Interview\Exceptions\Loan\FactoryException;
use PragmaGoTech\Interview\Factories\Loan\Factories\FeeFactoryInterface as FactoryInterface;
use PragmaGoTech\Interview\Models\Loan\ProposalModel;

/**
 * Factory of Loan Fee
 */
class FeeFactory implements FeeFactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function getLoanFeeFactoryByProposalModel(ProposalModel $proposalModel): FactoryInterface
    {
        /** @var FactoryInterface $classFactory */
        foreach (self::CLASS_FACTORIES as $classFactory) {
            $classFactory = new $classFactory();

            if ($classFactory->isSupported($proposalModel)) {
                return (new $classFactory())
                    ->setProposalModel($proposalModel);
            }
        }

        throw new FactoryException('Not exist supported factory');
    }
}