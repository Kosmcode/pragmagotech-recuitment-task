<?php

declare(strict_types=1);

namespace PragmaGoTech\Interview\Factories\Loan\Factories;

use PragmaGoTech\Interview\Enums\Loan\TermEnum;

class Months12AmountFrom3000To4000Factory extends AbstractFactory
{
    /**
     * {@inheritDoc}
     */
    protected function getLoanRate(): float
    {
        return -0.000125 * $this->getProposalModel()->getLoanAmount() + 3.375;
    }

    /**
     * {@inheritDoc}
     */
    protected function getExpectedTermEnum(): TermEnum
    {
        return TermEnum::months12;
    }

    /**
     * {@inheritDoc}
     */
    protected function getExpectedLoanAmountFrom(): int
    {
        return 3000;
    }

    /**
     * {@inheritDoc}
     */
    protected function getExpectedLoanAmountTo(): int
    {
        return 4000;
    }
}