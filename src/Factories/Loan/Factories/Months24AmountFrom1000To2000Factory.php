<?php

declare(strict_types=1);

namespace PragmaGoTech\Interview\Factories\Loan\Factories;

use PragmaGoTech\Interview\Enums\Loan\AmountEnum;
use PragmaGoTech\Interview\Enums\Loan\TermEnum;

class Months24AmountFrom1000To2000Factory extends AbstractFactory
{
    /**
     * {@inheritDoc}
     */
    protected function getLoanRate(): float
    {
        return -0.003 * $this->getProposalModel()->getLoanAmount() + 10;
    }

    /**
     * {@inheritDoc}
     */
    protected function getExpectedTermEnum(): TermEnum
    {
        return TermEnum::months24;
    }

    /**
     * {@inheritDoc}
     */
    protected function getExpectedLoanAmountFrom(): int
    {
        return AmountEnum::minLoanAmount->value;
    }

    /**
     * {@inheritDoc}
     */
    protected function getExpectedLoanAmountTo(): int
    {
        return 2000;
    }
}