<?php

declare(strict_types=1);

namespace PragmaGoTech\Interview\Factories\Loan\Factories;

use PragmaGoTech\Interview\Enums\Loan\TermEnum;
use PragmaGoTech\Interview\Models\Loan\ProposalModel;

/**
 * Abstract class of Fee Factory
 */
abstract class AbstractFactory implements FeeFactoryInterface
{
    /**
     * @var ProposalModel
     */
    protected ProposalModel $proposalModel;

    /** {@inheritDoc} */
    public function isSupported(ProposalModel $proposalModel): bool
    {
        return $proposalModel->getLoanTermEnum() === $this->getExpectedTermEnum()
            && $proposalModel->getLoanAmount() >= $this->getExpectedLoanAmountFrom()
            && $proposalModel->getLoanAmount() < $this->getExpectedLoanAmountTo();
    }

    /** {@inheritDoc} */
    public function calculateFee(): float
    {
        return $this->getProposalModel()->getLoanAmount() * ($this->getLoanRate() / 100);
    }

    /**
     * @return ProposalModel
     */
    public function getProposalModel(): ProposalModel
    {
        return $this->proposalModel;
    }

    /**
     * @param ProposalModel $proposalModel
     * @return $this
     */
    public function setProposalModel(ProposalModel $proposalModel): static
    {
        $this->proposalModel = $proposalModel;

        return $this;
    }

    /**
     * @return TermEnum
     */
    abstract protected function getExpectedTermEnum(): TermEnum;

    /**
     * @return int
     */
    abstract protected function getExpectedLoanAmountFrom(): int;

    /**
     * @return int
     */
    abstract protected function getExpectedLoanAmountTo(): int;

    /**
     * @return float
     */
    abstract protected function getLoanRate(): float;
}