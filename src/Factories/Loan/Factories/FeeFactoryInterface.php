<?php

declare(strict_types=1);

namespace PragmaGoTech\Interview\Factories\Loan\Factories;

use PragmaGoTech\Interview\Models\Loan\ProposalModel;

/**
 * Interface of Fee Factory
 */
interface FeeFactoryInterface
{
    /**
     * @param ProposalModel $proposalModel
     *
     * @return bool
     */
    public function isSupported(ProposalModel $proposalModel): bool;

    /**
     * @return float
     */
    public function calculateFee(): float;
}