<?php

declare(strict_types=1);

namespace PragmaGoTech\Interview\Factories\Loan\Factories;

use PragmaGoTech\Interview\Enums\Loan\TermEnum;

class Months12AmountFrom4000To5000Factory extends AbstractFactory
{
    /**
     * {@inheritDoc}
     */
    protected function getLoanRate(): float
    {
        return -0.000875 * $this->getProposalModel()->getLoanAmount() + 6.375;
    }

    /**
     * {@inheritDoc}
     */
    protected function getExpectedTermEnum(): TermEnum
    {
        return TermEnum::months12;
    }

    /**
     * {@inheritDoc}
     */
    protected function getExpectedLoanAmountFrom(): int
    {
        return 4000;
    }

    /**
     * {@inheritDoc}
     */
    protected function getExpectedLoanAmountTo(): int
    {
        return 5000;
    }
}