<?php

declare(strict_types=1);

namespace PragmaGoTech\Interview\Factories\Loan\Factories;

use PragmaGoTech\Interview\Enums\Loan\TermEnum;

class Months24AmountFrom2000To3000Factory extends AbstractFactory
{
    /**
     * {@inheritDoc}
     */
    protected function getLoanRate(): float
    {
        return -0.001 * $this->getProposalModel()->getLoanAmount() + 7;
    }

    /**
     * {@inheritDoc}
     */
    protected function getExpectedTermEnum(): TermEnum
    {
        return TermEnum::months24;
    }

    /**
     * {@inheritDoc}
     */
    protected function getExpectedLoanAmountFrom(): int
    {
        return 2000;
    }

    /**
     * {@inheritDoc}
     */
    protected function getExpectedLoanAmountTo(): int
    {
        return 3000;
    }
}