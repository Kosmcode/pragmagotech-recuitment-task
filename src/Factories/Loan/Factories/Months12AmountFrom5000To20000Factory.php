<?php

declare(strict_types=1);

namespace PragmaGoTech\Interview\Factories\Loan\Factories;

use PragmaGoTech\Interview\Enums\Loan\AmountEnum;
use PragmaGoTech\Interview\Enums\Loan\TermEnum;
use PragmaGoTech\Interview\Models\Loan\ProposalModel;

class Months12AmountFrom5000To20000Factory extends AbstractFactory
{
    /** {@inheritDoc} */
    public function isSupported(ProposalModel $proposalModel): bool
    {
        return $proposalModel->getLoanTermEnum() === $this->getExpectedTermEnum()
            && $proposalModel->getLoanAmount() >= $this->getExpectedLoanAmountFrom()
            && $proposalModel->getLoanAmount() <= $this->getExpectedLoanAmountTo();
    }

    /**
     * {@inheritDoc}
     */
    protected function getLoanRate(): float
    {
        return 2;
    }

    /**
     * {@inheritDoc}
     */
    protected function getExpectedTermEnum(): TermEnum
    {
        return TermEnum::months12;
    }

    /**
     * {@inheritDoc}
     */
    protected function getExpectedLoanAmountFrom(): int
    {
        return 5000;
    }

    /**
     * {@inheritDoc}
     */
    protected function getExpectedLoanAmountTo(): int
    {
        return AmountEnum::maxLoanAmount->value;
    }
}