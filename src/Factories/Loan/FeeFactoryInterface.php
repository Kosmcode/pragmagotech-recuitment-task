<?php

declare(strict_types=1);

namespace PragmaGoTech\Interview\Factories\Loan;

use PragmaGoTech\Interview\Exceptions\Loan\FactoryException;
use PragmaGoTech\Interview\Factories\Loan\Factories\FeeFactoryInterface as FactoryInterface;
use PragmaGoTech\Interview\Factories\Loan\Factories\Months12AmountFrom1000To2000Factory;
use PragmaGoTech\Interview\Factories\Loan\Factories\Months12AmountFrom2000To3000Factory;
use PragmaGoTech\Interview\Factories\Loan\Factories\Months12AmountFrom3000To4000Factory;
use PragmaGoTech\Interview\Factories\Loan\Factories\Months12AmountFrom4000To5000Factory;
use PragmaGoTech\Interview\Factories\Loan\Factories\Months12AmountFrom5000To20000Factory;
use PragmaGoTech\Interview\Factories\Loan\Factories\Months24AmountFrom1000To2000Factory;
use PragmaGoTech\Interview\Factories\Loan\Factories\Months24AmountFrom2000To3000Factory;
use PragmaGoTech\Interview\Factories\Loan\Factories\Months24AmountFrom3000To20000Factory;
use PragmaGoTech\Interview\Models\Loan\ProposalModel;

interface FeeFactoryInterface
{
    /**
     * @var array<int, FeeFactoryInterface>
     */
    const array CLASS_FACTORIES = [
        Months12AmountFrom1000To2000Factory::class,
        Months12AmountFrom2000To3000Factory::class,
        Months12AmountFrom3000To4000Factory::class,
        Months12AmountFrom4000To5000Factory::class,
        Months12AmountFrom5000To20000Factory::class,
        Months24AmountFrom1000To2000Factory::class,
        Months24AmountFrom2000To3000Factory::class,
        Months24AmountFrom3000To20000Factory::class,
    ];

    /**
     * @param ProposalModel $proposalModel
     *
     * @return FactoryInterface
     *
     * @throws FactoryException
     */
    public function getLoanFeeFactoryByProposalModel(ProposalModel $proposalModel): FactoryInterface;
}