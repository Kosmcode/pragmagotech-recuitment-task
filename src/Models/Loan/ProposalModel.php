<?php

declare(strict_types=1);

namespace PragmaGoTech\Interview\Models\Loan;

use PragmaGoTech\Interview\Enums\Loan\TermEnum;

/**
 * Model of Loan Proposal
 */
class ProposalModel
{
    /**
     * @param TermEnum $loanTermEnum
     * @param float $loanAmount
     */
    public function __construct(
        private TermEnum $loanTermEnum,
        private float    $loanAmount
    )
    {
    }

    /**
     * @return TermEnum
     */
    public function getLoanTermEnum(): TermEnum
    {
        return $this->loanTermEnum;
    }

    /**
     * @param TermEnum $loanTermEnum
     * @return $this
     */
    public function setLoanTermEnum(TermEnum $loanTermEnum): static
    {
        $this->loanTermEnum = $loanTermEnum;

        return $this;
    }

    /**
     * @return float
     */
    public function getLoanAmount(): float
    {
        return $this->loanAmount;
    }

    /**
     * @param float $loanAmount
     * @return $this
     */
    public function setLoanAmount(float $loanAmount): static
    {
        $this->loanAmount = $loanAmount;

        return $this;
    }
}
