<?php

declare(strict_types=1);

namespace PragmaGoTech\Interview\Validators\Loan;

use PragmaGoTech\Interview\Enums\Loan\AmountEnum;
use PragmaGoTech\Interview\Exceptions\Loan\AmountException;

class AmountValidator
{
    /**
     * @throws AmountException
     */
    public static function validate(float $amount): void
    {
        if ($amount > AmountEnum::maxLoanAmount->value) {
            throw new AmountException('Loan amount is too high');
        }

        if ($amount < AmountEnum::minLoanAmount->value) {
            throw new AmountException('Loan amount is too low');
        }
    }
}