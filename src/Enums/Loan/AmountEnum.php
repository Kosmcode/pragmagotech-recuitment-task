<?php

namespace PragmaGoTech\Interview\Enums\Loan;

enum AmountEnum: int
{
    case minLoanAmount = 1000;
    case maxLoanAmount = 20000;
}
