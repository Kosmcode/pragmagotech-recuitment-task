<?php

declare(strict_types=1);

namespace PragmaGoTech\Interview\Enums\Loan;

/**
 * Loan Term Enum
 */
enum TermEnum: int
{
    case months12 = 12;
    case months24 = 24;
}