<?php

declare(strict_types=1);

namespace PragmaGoTech\Interview\Services\Loan\Fee;

use PragmaGoTech\Interview\Exceptions\Loan\AmountException;
use PragmaGoTech\Interview\Exceptions\Loan\FactoryException;
use PragmaGoTech\Interview\Models\Loan\ProposalModel;

interface CalculatorInterface
{
    const int FEE_ROUND_CONDITION = 5;

    /**
     * @return float The calculated total fee.
     *
     * @throws AmountException
     * @throws FactoryException
     */
    public function calculate(ProposalModel $proposalModel): float;
}
