<?php

declare(strict_types=1);

namespace PragmaGoTech\Interview\Services\Loan\Fee;

use PragmaGoTech\Interview\Factories\Loan\FeeFactory;
use PragmaGoTech\Interview\Models\Loan\ProposalModel;
use PragmaGoTech\Interview\Traits\Number\RoundTrait;
use PragmaGoTech\Interview\Validators\Loan\AmountValidator;

/**
 * Service of Fee Calculator
 */
class CalculatorService implements CalculatorInterface
{
    use RoundTrait;

    /** {@inheritDoc} */
    public function calculate(ProposalModel $proposalModel): float
    {
        AmountValidator::validate($proposalModel->getLoanAmount());

        $feeFactory = (new FeeFactory())
            ->getLoanFeeFactoryByProposalModel($proposalModel);

        $loanFee = $feeFactory->calculateFee();

        return $this->roundUpToAny($loanFee, self::FEE_ROUND_CONDITION);
    }
}