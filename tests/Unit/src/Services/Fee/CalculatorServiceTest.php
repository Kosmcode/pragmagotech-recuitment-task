<?php

namespace Tests\Unit\Services\Fee;

use PragmaGoTech\Interview\Enums\Loan\AmountEnum;
use PragmaGoTech\Interview\Exceptions\Loan\AmountException;
use PragmaGoTech\Interview\Enums\Loan\TermEnum;
use PragmaGoTech\Interview\Exceptions\Loan\FactoryException;
use PragmaGoTech\Interview\Models\Loan\ProposalModel;
use PragmaGoTech\Interview\Services\Loan\Fee\CalculatorService;
use Tests\Unit\TestCase;

class CalculatorServiceTest extends TestCase
{
    const array TESTS_VALUES_FOR_12_MOUTHS = [
        1000 => 50,
        1500 => 75,
        1999 => 90,
        2000 => 90,
        2500 => 95,
        2999 => 95,
        3000 => 90,
        3500 => 105,
        3999 => 115,
        4000 => 115,
        4500 => 110,
        4999 => 105,
        5000 => 100,
        6000 => 120,
        6006 => 125,
        7000 => 140,
        8000 => 160,
        9000 => 180,
        10000 => 200,
        11000 => 220,
        12000 => 240,
        13000 => 260,
        14000 => 280,
        14666 => 295,
        15000 => 300,
        16000 => 320,
        17000 => 340,
        18000 => 360,
        19000 => 380,
        19250 => 385,
        20000 => 400,
    ];

    const array TESTS_VALUES_FOR_24_MOUTHS = [
        1000 => 70,
        1500 => 85,
        1999 => 85,
        2000 => 100,
        2500 => 115,
        2999 => 120,
        3000 => 120,
        4000 => 160,
        4005 => 165,
        5000 => 200,
        6000 => 240,
        7000 => 280,
        8000 => 320,
        9000 => 360,
        10000 => 400,
        10012 => 405,
        11000 => 440,
        11500 => 460,
        12000 => 480,
        13000 => 520,
        14000 => 560,
        15000 => 600,
        16000 => 640,
        17000 => 680,
        18000 => 720,
        19000 => 760,
        20000 => 800,
    ];

    public function testCalculateWhenLoanAmountIsTooLow(): void
    {
        $feeCalculatorService = new CalculatorService();
        $loanProposalModel = new ProposalModel(
            TermEnum::months12,
            AmountEnum::minLoanAmount->value - 1
        );

        $this->expectException(AmountException::class);
        $this->expectExceptionMessage('Loan amount is too low');

        $feeCalculatorService->calculate($loanProposalModel);
    }

    public function testCalculateWhenLoanAmountIsTooHigh(): void
    {
        $feeCalculatorService = new CalculatorService();
        $loanProposalModel = new ProposalModel(
            TermEnum::months12,
            AmountEnum::maxLoanAmount->value + 1
        );

        $this->expectException(AmountException::class);
        $this->expectExceptionMessage('Loan amount is too high');

        $feeCalculatorService->calculate($loanProposalModel);
    }

    public function testCalculateFor12Mouths(): void
    {
        $this->makeCalculateTestByLoanTermEnumAndValuesArray(
            TermEnum::months12,
            self::TESTS_VALUES_FOR_12_MOUTHS
        );
    }

    public function testCalculateFor24Mouths(): void
    {
        $this->makeCalculateTestByLoanTermEnumAndValuesArray(
            TermEnum::months24,
            self::TESTS_VALUES_FOR_24_MOUTHS
        );
    }

    /**
     * @throws AmountException|FactoryException
     */
    private function makeCalculateTestByLoanTermEnumAndValuesArray(
        TermEnum $termEnum,
        array    $testValuesArray
    ): void
    {
        $feeCalculatorService = new CalculatorService();
        $loanProposalModel = new ProposalModel(
            $termEnum,
            0
        );

        foreach ($testValuesArray as $loanAmount => $loanFee) {
            $loanProposalModel->setLoanAmount($loanAmount);

            $result = $feeCalculatorService->calculate($loanProposalModel);

            $this->assertIsFloat($result);
            $this->assertEquals($loanFee, $result);
        }
    }
}