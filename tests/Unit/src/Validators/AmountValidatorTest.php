<?php

namespace Tests\Unit\src\Validators;

use PragmaGoTech\Interview\Exceptions\Loan\AmountException;
use PragmaGoTech\Interview\Validators\Loan\AmountValidator;
use PragmaGoTech\Interview\Enums\Loan\AmountEnum;
use Tests\Unit\TestCase;

class AmountValidatorTest extends TestCase
{
    public function testValidateWhenAmountIsTooLow(): void
    {
        $this->expectException(AmountException::class);
        $this->expectExceptionMessage('Loan amount is too low');

        AmountValidator::validate(
            AmountEnum::minLoanAmount->value - 1
        );
    }

    public function testValidateWhenAmountIsTooHigh(): void
    {
        $this->expectException(AmountException::class);
        $this->expectExceptionMessage('Loan amount is too high');

        AmountValidator::validate(
            AmountEnum::maxLoanAmount->value + 1
        );
    }
}