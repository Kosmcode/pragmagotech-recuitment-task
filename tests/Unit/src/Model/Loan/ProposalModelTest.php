<?php

namespace Tests\Unit\src\Model\Loan;

use PragmaGoTech\Interview\Enums\Loan\TermEnum;
use PragmaGoTech\Interview\Models\Loan\ProposalModel;
use Tests\Unit\TestCase;

class ProposalModelTest extends TestCase
{
    const TermEnum TEST_LOAN_TERM_ENUM = TermEnum::months12;
    const float TEST_LOAN_AMOUNT = 1000;

    public function testGetLoanTermEnum(): void
    {
        $proposalModel = $this->getProposalModel();

        $result = $proposalModel->getLoanTermEnum();

        $this->assertInstanceOf(TermEnum::class, $result);
        $this->assertSame(self::TEST_LOAN_TERM_ENUM, $result);
    }

    public function testSetLoanTermEnum(): void
    {
        $proposalModel = ($this->getProposalModel())
            ->setLoanTermEnum(TermEnum::months12);

        $result = $proposalModel->getLoanTermEnum();

        $this->assertInstanceOf(TermEnum::class, $result);
        $this->assertSame(TermEnum::months12, $result);

        $proposalModel->setLoanTermEnum(TermEnum::months24);

        $result = $proposalModel->getLoanTermEnum();

        $this->assertInstanceOf(TermEnum::class, $result);
        $this->assertSame(TermEnum::months24, $result);
    }

    public function testLoanGetAmount(): void
    {
        $proposalModel = $this->getProposalModel();

        $result = $proposalModel->getLoanAmount();

        $this->assertIsFloat($result);
        $this->assertSame(self::TEST_LOAN_AMOUNT, $result);
    }

    public function testLoanSetAmount(): void
    {
        $proposalModel = ($this->getProposalModel())
            ->setLoanAmount(999.99);

        $result = $proposalModel->getLoanAmount();

        $this->assertIsFloat($result);
        $this->assertSame(999.99, $result);

        $proposalModel->setLoanAmount(666);

        $result = $proposalModel->getLoanAmount();

        $this->assertIsFloat($result);
        $this->assertSame(666.0, $result);
    }

    private function getProposalModel(): ProposalModel
    {
        return new ProposalModel(
            self::TEST_LOAN_TERM_ENUM,
            self::TEST_LOAN_AMOUNT
        );
    }
}