<?php

namespace Tests\Unit\src\Factories\Loan\Factories;

use PragmaGoTech\Interview\Enums\Loan\TermEnum;
use PragmaGoTech\Interview\Factories\Loan\Factories\AbstractFactory;
use PragmaGoTech\Interview\Models\Loan\ProposalModel;
use Tests\Unit\TestCase;

class AbstractFactoryTest extends TestCase
{
    public function testIsSupportedWhenIs(): void
    {
        $abstractFactoryMock = $this->getMockForAbstractClass(AbstractFactory::class);
        $abstractFactoryMock
            ->method('getExpectedTermEnum')
            ->willReturn(TermEnum::months12);
        $abstractFactoryMock
            ->method('getExpectedLoanAmountFrom')
            ->willReturn(1000);
        $abstractFactoryMock
            ->method('getExpectedLoanAmountTo')
            ->willReturn(2000);

        $result = $abstractFactoryMock->isSupported(
            new ProposalModel(TermEnum::months12, 1500)
        );

        $this->assertIsBool($result);
        $this->assertTrue($result);
    }

    public function testIsSupportedWhenIsNot(): void
    {
        $abstractFactoryMock = $this->getMockForAbstractClass(AbstractFactory::class);

        $abstractFactoryMock
            ->method('getExpectedTermEnum')
            ->willReturn(TermEnum::months12);
        $abstractFactoryMock
            ->method('getExpectedLoanAmountFrom')
            ->willReturn(1000);
        $abstractFactoryMock
            ->method('getExpectedLoanAmountTo')
            ->willReturn(2000);

        $result = $abstractFactoryMock->isSupported(
            new ProposalModel(TermEnum::months12, 3000)
        );

        $this->assertIsBool($result);
        $this->assertFalse($result);
    }

    public function testCalculateFee(): void
    {
        $abstractFactoryMock = $this->getMockForAbstractClass(AbstractFactory::class);

        $abstractFactoryMock
            ->method('getLoanRate')
            ->willReturn(5.0);

        $abstractFactoryMock->setProposalModel(new ProposalModel(TermEnum::months12, 3000));

        $result = $abstractFactoryMock->calculateFee();

        $this->assertIsFloat($result);
        $this->assertSame(3000 * (5.0 / 100), $result);
    }

    public function testGetAndSetProposalModel(): void
    {
        $proposalModel = new ProposalModel(TermEnum::months12, 3000);

        $abstractFactoryMock = $this->getMockForAbstractClass(AbstractFactory::class);

        $abstractFactoryMock->setProposalModel($proposalModel);

        $result = $abstractFactoryMock->getProposalModel();

        $this->assertInstanceOf(ProposalModel::class, $result);
        $this->assertSame($proposalModel, $result);
    }
}