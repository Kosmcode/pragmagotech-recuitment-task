<?php

namespace Tests\Unit\src\Factories\Loan;

use PragmaGoTech\Interview\Enums\Loan\AmountEnum;
use PragmaGoTech\Interview\Enums\Loan\TermEnum;
use PragmaGoTech\Interview\Exceptions\Loan\FactoryException;
use PragmaGoTech\Interview\Factories\Loan\Factories\FeeFactoryInterface as FactoryInterface;
use PragmaGoTech\Interview\Factories\Loan\FeeFactory;
use PragmaGoTech\Interview\Models\Loan\ProposalModel;
use Tests\Unit\TestCase;

class FeeFactoryTest extends TestCase
{
    public function testGetLoanFeeFactoryByProposalModelWhenSupportedFactoryExist(): void
    {
        $feeFactory = new FeeFactory();

        $result = $feeFactory->getLoanFeeFactoryByProposalModel(
            new ProposalModel(TermEnum::months12, AmountEnum::maxLoanAmount->value)
        );

        $this->assertInstanceOf(FactoryInterface::class, $result);
    }

    public function testGetLoanFeeFactoryByProposalModelWhenSupportedFactoryNotExist(): void
    {
        $feeFactory = new FeeFactory();

        $this->expectException(FactoryException::class);
        $this->expectExceptionMessage('Not exist supported factory');

        $result = $feeFactory->getLoanFeeFactoryByProposalModel(
            new ProposalModel(TermEnum::months12, 0)
        );

        $this->assertInstanceOf(FactoryInterface::class, $result);
    }
}